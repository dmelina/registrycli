package main

import (
	"os"

	"gitlab.com/dmelina/registrycli"
)

const (
	successColor = "\x1b[32m"
	failColor    = "\x1b[31m"
	varColor     = "\x1b[33m"
	reset        = "\x1b[m"
)

func main() {
	app := registrycli.NewApp()

	// Commands List
	//app.Before = func(c *cli.Context) error {
	//	for _, name := range c.GlobalFlagNames() {
	//		fmt.Printf("Global FlagsName: %s -> %s%s%s\n", name, successColor, c.GlobalString(name), reset)
	//	}
	//	return nil
	//}
	// Main action
	//app.Action = func(c *cli.Context) {
	//	fmt.Printf("ARGS: %#v\n", c.Args())
	//}

	app.Run(os.Args)
}
