package registrycli

import "github.com/codegangsta/cli"

const (
	// Name represents the registry client name
	Name = "registrycli"
	// Usage reprensents the command line usage
	Usage = "Docker registry client"
	// Version represents the registry client version
	Version = "0.1"
)

var (
	// Authors represents the authors list
	Authors = []cli.Author{
		cli.Author{"Davy MELINA", "davy.melina@gmail.com"},
	}
)

// RegistryClient represents the Docker Registry client
type RegistryClient struct {
	RegistryServer string
	Repos          []*DockerRepository
	Err            error
}

// DockerRepository represents the Docker repositories
type DockerRepository struct {
	Name string
	Tag  string
}

// NewApp represent the codegangsta CLI
func NewApp() *cli.App {
	app := cli.NewApp()
	app.Name = Name
	app.Usage = Usage
	app.Version = Version
	app.Authors = Authors
	app.Flags = GlobalFlagSet
	app.Before = func(c *cli.Context) error {
		return nil
	}
	//app.CommandNotFound = func(c *cli.Context, command string) {
	//	fmt.Printf("Command not found: wtf\n")
	//}
	app.Commands = []cli.Command{
		imagesCommand,
	}
	return app
}
