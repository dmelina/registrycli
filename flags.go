package registrycli

import "github.com/codegangsta/cli"

// Flags for setting the CLI
var (
	// GlobalFlagSet represents the global flags
	GlobalFlagSet = []cli.Flag{
		cli.StringFlag{Name: "registry-server, r", Usage: "registry-server", Value: "http://localhost:5000", EnvVar: "REGISTRY_SERVER"},
	}
)
