package registrycli

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"

	"github.com/codegangsta/cli"
)

var (
	imagesCommand = cli.Command{
		Name:        "images",
		Usage:       "List images",
		Description: "This command lists the images stored in the Docker repository.",
		Action: func(c *cli.Context) {
			fmt.Printf("---> %s\n", c.GlobalString("registry-server"))
			client := &RegistryClient{
				RegistryServer: c.GlobalString("registry-server"),
			}
			repos, err := client.GetRepos()
			if err != nil {
				fmt.Printf("Client Error: %s\n", err)
			}
			PrintRepos(repos)
		},
	}
)

// GetRepos return the docker images list
func (cl *RegistryClient) GetRepos() ([]*DockerRepository, error) {
	resp, err := http.Get(fmt.Sprintf("%s/v2/_catalog", cl.RegistryServer))
	defer resp.Body.Close()
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	data := map[string]interface{}{}
	if err := json.Unmarshal([]byte(body), &data); err != nil {
		return nil, err
	}

	all := []*DockerRepository{}
	repos := data["repositories"].([]interface{})

	var wg sync.WaitGroup

	for _, name := range repos {
		wg.Add(1)
		repo := &DockerRepository{Name: name.(string)}
		go func() {
			tags, _ := cl.GetTags(repo.Name)
			//if err != nil {
			//	cl.Err = err
			//}
			for _, tag := range tags {
				repo.Tag = tag
			}
			all = append(all, repo)
			wg.Done()
		}()
	}
	wg.Wait()
	//if cl.Err != nil {
	//	return nil, cl.Err
	//}
	return all, nil
}

// GetTags return image tags listing
func (cl *RegistryClient) GetTags(name string) ([]string, error) {
	resp, err := http.Get(fmt.Sprintf("%s/v2/%s/tags/list", cl.RegistryServer, name))
	defer resp.Body.Close()
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	data := map[string]interface{}{}
	if err := json.Unmarshal([]byte(body), &data); err != nil {
		return nil, err
	}
	all := []string{}
	tags := data["tags"].([]interface{})
	for _, tag := range tags {
		all = append(all, tag.(string))
	}
	return all, nil
}

// PrintRepos print repositories list
func PrintRepos(repos []*DockerRepository) {
	fmt.Printf("%-32s%s\n", "REPOSITORY", "TAG")
	for _, repo := range repos {
		fmt.Printf("%-32s%s\n", repo.Name, repo.Tag)
	}
}
